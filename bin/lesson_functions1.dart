// ignore_for_file: unused_local_variable

void main(List<String> arguments) {
  print(function(2));
  print(funcTwo(3));
  print(functThree(a: 5, b: 4));
  print(funcFour(f: 8, t: 4, k: 2));
  print(funcFive(min: 2));
  List array = [1, 3, 4, 5, 6];
  print(funcSix(x: array));

  var p = true;
  print(funcSeven(p: p));

  int t = 1;
  print(funcEight(t: 1));

//9.
  List<int> list = [1, 2, 3, 3, 4];
  int myfunc = 0;
  bool isConains = false;

  list.forEach((e) {
    if (myfunc == e) {
      isConains = true;
    }
    myfunc = e;
  });
  if (isConains == true) {
    print('da');
  } else {
    print('net');
  }
}

//1.Сделайте функцию, которая возвращает квадрат числа. Число передается параметром.
int function(int a) {
  int b = a * a;
  return b;
}

//2.Сделайте функцию, которая возвращает сумму двух чисел.
int funcTwo(int c) {
  int d = c + c;
  return d;
}

//3.Сделайте функцию, которая отнимает от первого числа второе и делит на третье.
int functThree({required int a, required int b}) {
  return a - b;
}

//4.Сделайте функцию, которая отнимает от первого числа второе и делит на третье.
double funcFour({required int f, required int t, required int k}) {
  return (f - t) / k;
}

//5.Напишите функцию, которая принимает целые минуты и преобразует их в секунды.
String funcFive({required int min}) {
  return '$min mins = ${min * 60} secs';
}

//6.Есть массив, List array = [1, 3, 4, 5, 6]; возвратите его первый элемент
int funcSix({required List x}) {
  return x[0];
}

//7.Создайте bool переменную и напишите условие (if…else), выведите сообщение «Переменная имеет значение (значение вашей переменной)
String funcSeven({required bool p}) {
  if (p is bool) {
    return ('Переменная имеет значение true');
  } else {
    return 'false';
  }
}

//8.Создайте функцию, которая принимает число в качестве единственного аргумента и возвращает true,
//если оно меньше или равно нулю, в противном случае возвращает false.
bool funcEight({required int t}) {
  if (t <= 0) {
    return true;
  } else {
    return false;
  }
}
